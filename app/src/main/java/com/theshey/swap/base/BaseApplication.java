package com.theshey.swap.base;

import android.app.Activity;
import android.app.Application;

import com.androidnetworking.AndroidNetworking;
import com.blankj.utilcode.util.Utils;
import com.squareup.leakcanary.LeakCanary;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;


/**
 * @Author: TheShey
 * @Created at: 2017/7/26 14:38
 * @Email: shilishihe@gmail.com
 * @WeChat: Sonher_Lee
 * @Note: App基类
 */
public class BaseApplication extends Application {

    private List<Activity> activitys;
    private static BaseApplication appContext;
    private OkHttpClient okHttpClient;

    public static BaseApplication getThisApp() {
        return appContext;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        if (LeakCanary.isInAnalyzerProcess(this)) {
            return;
        }
        appContext = this;
        initOkHttpClient();
        AndroidNetworking.initialize(this, okHttpClient);
        LeakCanary.install(this);
        initViseLog();
        initConfigHttp();
        Utils.init(this);
        activitys = new LinkedList<>();
    }

    private OkHttpClient initOkHttpClient() {
        if (okHttpClient == null) {
            HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
            httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            okHttpClient = new OkHttpClient().newBuilder()
                    .addNetworkInterceptor(httpLoggingInterceptor)
                    .build();
        }
        return okHttpClient;
    }


    public void addActivity(Activity activity) {
        activitys.add(activity);
    }

    public void removeActivity(Activity activity) {
        if (activity != null && activitys.contains(activity)) {
            activitys.remove(activity);
            activity.finish();
        }
    }

    /**
     * 获取最后添加的一个Activity
     */
    public Activity getLastActivity() {
        if (activitys != null && activitys.size() > 0) {
            return activitys.get(activitys.size() - 1);
        }
        return null;
    }

    /**
     * 结束指定的Class
     *
     * @param cls
     */
    public void finishActivity(Class<?> cls) {
        Iterator<Activity> iterator = activitys.iterator();
        while (iterator.hasNext()) {
            Activity activity = iterator.next();
            if (activity.getClass().equals(cls)) {
                iterator.remove();
                activity.finish();
            }
        }
    }

    /**
     * Finish当前Activity
     */
    public void finishActivityExLast() {
        if (activitys != null && activitys.size() > 0) {
            for (int i = activitys.size() - 1; i > 0; i--) {
                Activity activity = activitys.get(i);
                activitys.remove(activity);
                activity.finish();
            }
        }
    }

    /**
     * Finish所有Activity
     */
    public void finishAllActivity() {
        Iterator<Activity> iterator = activitys.iterator();
        while (iterator.hasNext()) {
            Activity activity = iterator.next();
            if (activity != null) {
                iterator.remove();
                activity.finish();
            }
        }
    }

    public void exitApp() {
        finishAllActivity();
        android.os.Process.killProcess(android.os.Process.myPid());
        System.exit(0);
    }

    private void initViseLog() {
       /* ViseLog.plant(new LogcatTree());//添加打印日志信息到Logcat的树
        ViseLog.getLogConfig()
                .configAllowLog(true)//是否输出日志
                .configShowBorders(false);//是否排版显示*/
    }

    /**
     * 初始化网络请求框架
     */
    private void initConfigHttp() {
       /* ViseHttp.init(this);
        ViseHttp.CONFIG()
                //配置请求主机地址
                .baseUrl("http://192.168.1.100/")
                //配置全局请求头
                .globalHeaders(new HashMap<String, String>())
                //配置全局请求参数
                .globalParams(new HashMap<String, String>())
                //配置读取超时时间，单位秒
                .readTimeout(30)
                //配置写入超时时间，单位秒
                .writeTimeout(30)
                //配置连接超时时间，单位秒
                .connectTimeout(30)
                //配置请求失败重试次数
                .retryCount(3)
                //配置请求失败重试间隔时间，单位毫秒
                .retryDelayMillis(1000)
                //配置是否使用cookie
                .setCookie(false)
                // 不开启缓存机制
                .setHttpCache(false)
                //配置应用级拦截器
                .interceptor(new HttpLogInterceptor().setLevel(HttpLogInterceptor.Level.BODY))
                //配置转换工厂
                .converterFactory(GsonConverterFactory.create())
                //配置适配器工厂
                .callAdapterFactory(RxJava2CallAdapterFactory.create())
                //配置SSL证书验证
                .SSLSocketFactory(SSLUtil.getSslSocketFactory(null, null, null));*/
    }


}
