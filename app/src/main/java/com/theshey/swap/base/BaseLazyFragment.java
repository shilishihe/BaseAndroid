package com.theshey.swap.base;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

/**
 * @Author: TheShey
 * @Created at: 2018/4/21 18:03
 * @Email: shilishihe@gmail.com
 * @WeChat: Sonher_Lee
 * @Note: 懒加载Fragment, 可用作于需要懒加载Fragment的基类
 */
public abstract class BaseLazyFragment extends BaseFragment {

    protected boolean isHintVisible;  // 视图是否可见
    protected boolean isViewInit; // 布局初始化是否完成

    @Override
    protected int getContentViewLayoutID() {
        return Integer.MAX_VALUE;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        isViewInit = true;
        isCanLoadData();
        super.onViewCreated(view, savedInstanceState);
    }

    /**
     * 视图是否已经对用户可见，系统的方法
     */
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        isCanLoadData();
    }

    /**
     * 是否可以加载数据
     * 可以加载数据的条件：
     * 1.视图已经初始化
     * 2.视图对用户可见
     */
    private void isCanLoadData() {
        if (!isViewInit) {
            return;
        }
        if (getUserVisibleHint()) {
            lazyLoad();
            isHintVisible = true;
        } else {
            if (isHintVisible) {
                stopLoad();
            }
        }
    }

    /**
     * 当视图初始化并且对用户可见的时候去真正的加载数据
     */
    protected abstract void lazyLoad();

    /**
     * 当视图已经对用户不可见并且加载过数据，如果需要在切换到其他页面时停止加载数据，可以覆写此方法
     */
    protected void stopLoad() {
    }

    /**
     * 视图销毁的时候讲Fragment是否初始化的状态变为false
     */
    @Override
    public void onDestroyView() {
        isHintVisible = false;
        isViewInit = false;
        super.onDestroyView();
    }

}
