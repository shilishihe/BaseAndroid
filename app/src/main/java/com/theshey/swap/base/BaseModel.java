package com.theshey.swap.base;

import java.util.Map;

/**
 * @Author: TheShey
 * @Created at: 2017/7/27 9:29
 * @Email: shilishihe@gmail.com
 * @WeChat: Sonher_Lee
 * @Note: Model基类
 */
public class BaseModel {

    // 请求参数
    protected Map<String, String> params;

    /**
     * Get方法构造带参数的请求地址
     *
     * @param oriUrl
     * @param params
     * @return
     */
    protected String buildGetUrl(String oriUrl, Map<String, String> params) {
        oriUrl = oriUrl + "?";
        for (Map.Entry<String, String> entry : params.entrySet()) {
            oriUrl = oriUrl + entry.getKey() + "=" + entry.getValue() + "&";
        }
        return oriUrl.substring(0, oriUrl.length() - 1);
    }
}
