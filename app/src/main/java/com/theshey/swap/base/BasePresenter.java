package com.theshey.swap.base;

/**
 * @Author: TheShey
 * @Created at: 2017/7/26 16:49
 * @Email: shilishihe@gmail.com
 * @WeChat: Sonher_Lee
 * @Note: describe your meaning
 */
public abstract class BasePresenter<V extends IBaseView> {

    public BasePresenter() {
    }

    public BasePresenter(V mvpIView) {
        this.attachIView(mvpIView);
    }

    /**
     * 构建当前请求的TAG
     *
     * @param methodName
     * @return
     */
    public String buildRequestTag(String methodName) {
        return this.getClass().getCanonicalName() + methodName;
    }

    /**
     * 绑定的view
     */
    private V mvpIView;

    /**
     * 绑定mvpIView，一般在初始化中调用该方法
     */
    public void attachIView(V mvpIView) {
        this.mvpIView = mvpIView;
    }

    /**
     * 断开view，一般在onDestroy中调用
     */
    public void detachIView() {
        this.mvpIView = null;
    }

    /**
     * 是否与mvpIView建立连接
     * 每次调用业务请求的时候都要出先调用方法检查是否与mvpIView建立连接
     */
    public boolean isIViewAttached() {
        return mvpIView != null;
    }

    /**
     * 获取连接的view
     */
    public V getIView() {
        return mvpIView;
    }

    /**
     * 取消所有网络请求
     */
    public abstract void onCancleAll();

}
