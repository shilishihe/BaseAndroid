package com.theshey.swap.domain;

import java.util.List;

/**
 * @Author: TheShey
 * @Created at: 2017/7/26 17:04
 * @Email: shilishihe@gmail.com
 * @WeChat: Sonher_Lee
 * @Note: 必应图片实体
 */
public class BingImagesBean {

    private String imgUrl;
    private String head;
    private String calendar;
    private String location;

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getHead() {
        return head;
    }

    public void setHead(String head) {
        this.head = head;
    }

    public String getCalendar() {
        return calendar;
    }

    public void setCalendar(String calendar) {
        this.calendar = calendar;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}
