package com.theshey.swap.domain.adapter;

import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.widget.ImageView;

import com.blankj.utilcode.util.SizeUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.theshey.swap.R;
import com.theshey.swap.domain.BingImagesBean;
import com.theshey.swap.pub.glide.GlideManager;

import java.util.List;

import jp.wasabeef.glide.transformations.RoundedCornersTransformation;


/**
 * @Author: TheShey
 * @Created at: 2017/7/27 14:45
 * @Email: shilishihe@gmail.com
 * @WeChat: Sonher_Lee
 * @Note: MainActivity 列表数据适配器
 */
public class MainListAdapter extends BaseQuickAdapter<BingImagesBean, BaseViewHolder> {

    public MainListAdapter(@LayoutRes int layoutResId, @Nullable List<BingImagesBean> data) {
        super(layoutResId, data);
    }

    public MainListAdapter(@Nullable List<BingImagesBean> data) {
        super(data);
    }

    public MainListAdapter(@LayoutRes int layoutResId) {
        super(layoutResId);
    }

    @Override
    protected void convert(BaseViewHolder helper, BingImagesBean item) {
        helper.setText(R.id.tvName, item.getHead()).setText(R.id.tvTimeStamp, item.getCalendar());
        ImageView imPic = helper.getView(R.id.imPic);
        GlideManager.loadUrlImg(imPic, item.getImgUrl()
                , new RoundedCornersTransformation(SizeUtils.dp2px(5F), 0, RoundedCornersTransformation.CornerType.BOTTOM));

    }
}
