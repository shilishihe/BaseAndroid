package com.theshey.swap.module.main;

import android.Manifest;
import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;

import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.miguelcatalan.materialsearchview.MaterialSearchView;
import com.scwang.smartrefresh.header.MaterialHeader;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.tbruyelle.rxpermissions2.Permission;
import com.theshey.swap.R;
import com.theshey.swap.base.BaseActivity;
import com.theshey.swap.domain.BingImagesBean;
import com.theshey.swap.domain.adapter.MainListAdapter;

import java.util.List;

import butterknife.BindView;
import io.reactivex.functions.Consumer;

public class MainActivity extends BaseActivity implements MainView, OnRefreshListener, BaseQuickAdapter.OnItemClickListener
        , BaseQuickAdapter.OnItemLongClickListener
        , BaseQuickAdapter.RequestLoadMoreListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.fab)
    FloatingActionButton fab;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.refreshLayout)
    SmartRefreshLayout refreshLayout;
    @BindView(R.id.search_view)
    MaterialSearchView searchView;
    private MainPresenter mainPresenter;
    private MainListAdapter mainListAdapter;
    private boolean isFirstEnter = true;
    private MaterialHeader mMaterialHeader;
    private int currentPage = 1;
    private int MAX_PAGE = 8;

    @Override
    protected int getContentViewLayoutID() {
        return R.layout.activity_main;
    }

    @Override
    protected void initActivityView() {
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        mMaterialHeader = (MaterialHeader) refreshLayout.getRefreshHeader();
        refreshLayout.setEnableHeaderTranslationContent(true);
        mMaterialHeader.setShowBezierWave(false);
        mMaterialHeader.setColorSchemeColors(getResources().getColor(R.color.colorPrimary));
    }

    @Override
    protected void initActivityData() {
        setSupportActionBar(toolbar);
        mainPresenter = new MainPresenter();
        mainPresenter.attachIView(this);
        mainListAdapter = new MainListAdapter(R.layout.activity_main_list_item);
        mainListAdapter.setOnLoadMoreListener(this, recyclerView);
        recyclerView.setAdapter(mainListAdapter);
        searchView.setSuggestions(getResources().getStringArray(R.array.query_suggestions));
        // 提交和变更监听回调
        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        // 隐藏和展示监听回调
        searchView.setOnSearchViewListener(new MaterialSearchView.SearchViewListener() {
            @Override
            public void onSearchViewShown() {
            }

            @Override
            public void onSearchViewClosed() {
            }
        });
        refreshLayout.setOnRefreshListener(this);
        mainListAdapter.setOnItemClickListener(this);
        mainListAdapter.setOnItemLongClickListener(this);
        registerForContextMenu(recyclerView);
    }

    /**
     * 测试向fabric提交用户操作记录
     */
    public void testUserActions() {
       /* Answers.getInstance().logContentView(new ContentViewEvent()
                .putContentName("Tweet")
                .putContentType("Video")
                .putContentId("1234")
                .putCustomAttribute("Favorites Count", 20)
                .putCustomAttribute("Screen Orientation", "Landscape"));*/
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        // 设置指定的扩展SearchView
        MenuItem item = menu.findItem(R.id.action_search);
        searchView.setMenuItem(item);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBingDataGeted(List<BingImagesBean> datas, boolean isRefresh) {
        refreshLayout.finishRefresh();
        currentPage++;
        LogUtils.e("currentPage=" + currentPage);
        mainListAdapter.loadMoreComplete();
        if (datas == null) {
            return;
        }
        int size = datas.size();
        if (size < MAX_PAGE) {
            mainListAdapter.loadMoreEnd(false);  // 不显示已无更多数据
        }
        if (currentPage > MAX_PAGE) {
            mainListAdapter.loadMoreEnd();
            LogUtils.e("mainListAdapter.loadMoreEnd()");
        }
        if (isRefresh) {
            mainListAdapter.setEnableLoadMore(true);
            mainListAdapter.setNewData(datas);
        } else {
            mainListAdapter.setEnableLoadMore(true);
            mainListAdapter.addData(datas);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onRefresh(RefreshLayout refreshlayout) {
        currentPage = 1;
        // 防止下拉刷新的时候还可以上拉加载
        mainListAdapter.setEnableLoadMore(false);
        mainPresenter.getBingImages(currentPage, true);
    }

    @Override
    public void onBackPressed() {
        if (searchView.isSearchOpen()) {
            searchView.closeSearch();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onItemClick(BaseQuickAdapter adapter, View view, int position) {

    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        getMenuInflater().inflate(R.menu.menu_float, menu);
        menu.setHeaderIcon(R.mipmap.ic_launcher);
        menu.setHeaderTitle("菜单标题");
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        if (info == null) {
            ToastUtils.showShort("AdapterContextMenuInfo is null");
        }
        return super.onContextItemSelected(item);
    }

    @Override
    public boolean onItemLongClick(BaseQuickAdapter adapter, View view, int position) {
        return false;
    }

    @SuppressLint("CheckResult")
    private void requestAllPermission() {
        /*rxPermissions.requestEach(Manifest.permission.CAMERA,
                Manifest.permission.READ_PHONE_STATE)
                .subscribe(new Consumer<Permission>() {
                    @Override
                    public void accept(Permission permission) throws Exception {

                    }
                });*/

        /*rxPermissions.request(Manifest.permission.CAMERA,
                Manifest.permission.READ_PHONE_STATE).subscribe(new Consumer<Boolean>() {
            @Override
            public void accept(Boolean aBoolean) throws Exception {
                if (aBoolean) {
                    // 用户已经同意该权限
                    LogUtils.eTag("用户已经同意该权限");
                } else {
                    // 用户拒绝了该权限，没有选中『不再询问』（Never ask again）,那么下次再次启动时，还会提示请求权限的对话框
                    LogUtils.eTag("用户拒绝了该权限");
                } *//*else {
                    // 用户拒绝了该权限，并且选中『不再询问』，提醒用户手动打开权限
                    LogUtils.eTag("权限被拒绝，请在设置里面开启相应权限，若无相应权限会影响使用");
                }*//*
            }
        });*/

        rxPermissions.requestEachCombined(Manifest.permission.CAMERA, Manifest.permission.READ_PHONE_STATE)
                .subscribe(new Consumer<Permission>() {
                    @Override
                    public void accept(Permission permission) throws Exception {
                        if (permission.granted) {
                            // 用户已经同意该权限
                            LogUtils.eTag("用户已经同意该权限");
                        } else if (permission.shouldShowRequestPermissionRationale) {
                            // 用户拒绝了该权限，没有选中『不再询问』（Never ask again）,那么下次再次启动时，还会提示请求权限的对话框
                            LogUtils.eTag("用户拒绝了该权限");
                        } else {
                            // 用户拒绝了该权限，并且选中『不再询问』，提醒用户手动打开权限
                            LogUtils.eTag("权限被拒绝，请在设置里面开启相应权限，若无相应权限会影响使用");
                        }
                    }
                });
    }

    @Override
    protected void onResume() {
        if (isFirstEnter) {
            isFirstEnter = false;
            refreshLayout.autoRefresh();
        }
        requestAllPermission();
        super.onResume();
    }

    @Override
    public void onLoadMoreRequested() {
        LogUtils.e("onLoadMoreRequested");
        mainPresenter.getBingImages(currentPage, false);
    }

    @Override
    public void showErr(String msg) {
        mainListAdapter.setEnableLoadMore(true);
        ToastUtils.showShort(msg);
    }
}
