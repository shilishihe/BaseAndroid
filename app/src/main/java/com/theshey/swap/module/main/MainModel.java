package com.theshey.swap.module.main;

import com.theshey.swap.base.BaseModel;
import com.theshey.swap.base.ICallback;
import com.theshey.swap.domain.BingImagesBean;
import com.theshey.swap.net.NetClient;

import java.util.List;

/**
 * @Author: TheShey
 * @Created at: 2017/7/27 9:37
 * @Email: shilishihe@gmail.com
 * @WeChat: Sonher_Lee
 * @Note:
 */
public class MainModel extends BaseModel {

    public static void getBingImgs(int page, final ICallback callback) {
        NetClient.getNetClient().getBingImages(page, callback);
    }
}
