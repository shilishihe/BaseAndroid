package com.theshey.swap.module.main;

import com.blankj.utilcode.util.LogUtils;
import com.theshey.swap.base.BasePresenter;
import com.theshey.swap.base.ICallback;
import com.theshey.swap.domain.BingImagesBean;

import java.util.List;

/**
 * @Author: TheShey
 * @Created at: 2017/7/26 17:02
 * @Email: shilishihe@gmail.com
 * @WeChat: Sonher_Lee
 * @Note:
 */
public class MainPresenter extends BasePresenter<MainView> {

    /**
     * 获取必应网图片
     */
    public void getBingImages(int page, final boolean isRefresh) {
        MainModel.getBingImgs(page, new ICallback<List<BingImagesBean>>() {

            @Override
            public void onSuccess(List<BingImagesBean> data) {
                LogUtils.i("请求成功哦");
                if (isIViewAttached()) {
                    getIView().onBingDataGeted(data, isRefresh);
                }
            }

            @Override
            public void onFailure(String msg) {
                if (isIViewAttached()) {
                    LogUtils.i("请求失败哦");
                    getIView().showErr(msg);
                }
            }

            @Override
            public void onComplete() {
                if (isIViewAttached()) {
                    getIView().closeLoading();
                }
            }
        });
    }


    @Override
    public void onCancleAll() {

    }
}
