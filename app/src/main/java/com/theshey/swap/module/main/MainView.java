package com.theshey.swap.module.main;

import com.theshey.swap.base.IBaseView;
import com.theshey.swap.domain.BingImagesBean;

import java.util.List;

/**
 * @Author: TheShey
 * @Created at: 2017/7/26 16:54
 * @Email: shilishihe@gmail.com
 * @WeChat: Sonher_Lee
 * @Note: describe your meaning
 */
public interface MainView extends IBaseView {

    void onBingDataGeted(List<BingImagesBean> datas,boolean isRefresh);

}
