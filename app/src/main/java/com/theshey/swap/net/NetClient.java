package com.theshey.swap.net;

import com.rx2androidnetworking.Rx2AndroidNetworking;
import com.theshey.swap.base.ICallback;
import com.theshey.swap.domain.BingImagesBean;

import org.htmlcleaner.CleanerProperties;
import org.htmlcleaner.DomSerializer;
import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.TagNode;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.List;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

/**
 * @Author: TheShey
 * @Created at: 2018/4/24 16:38
 * @Email: shilishihe@gmail.com
 * @WeChat: Sonher_Lee
 * @Note: 网络请求类
 */
public class NetClient {

    private NetClient() {
    }

    private static NetClient netClient = null;

    public static NetClient getNetClient() {
        if (netClient == null) {
            synchronized (NetClient.class) {
                if (netClient == null) {
                    netClient = new NetClient();
                }
            }
        }
        return netClient;
    }

    public void getBingImages(int page, final ICallback iCallback) {
        /*AndroidNetworking.get(RequestUrls.BING_IMAGES)
                .addQueryParameter("p", String.valueOf(page))
                .setTag("getBingImages")
                .build()
                .getAsOkHttpResponseAndString(new OkHttpResponseAndStringRequestListener() {
                    @Override
                    public void onResponse(Response okHttpResponse, String response) {
                        if (200 == okHttpResponse.code()) {  // 请求正常
                            List<BingImagesBean> beans = null;
                            try {
                                TagNode tagNode = new HtmlCleaner().clean(response);
                                org.w3c.dom.Document doc = new DomSerializer(new CleanerProperties()).createDOM(tagNode);
                                XPath xpath = XPathFactory.newInstance().newXPath();
                                Object object = xpath.evaluate("//div[@class='item']//div[@class='card progressive']", doc, XPathConstants.NODESET);
                                if (object instanceof NodeList) {
                                    NodeList nodes = (NodeList) object;
                                    beans = new ArrayList<>();
                                    if (nodes != null && nodes.getLength() > 0) {
                                        for (int i = 0; i < nodes.getLength(); i++) {
                                            Element element = (Element) nodes.item(i);
                                            Element element0 = (Element) element.getElementsByTagName("img").item(0);
                                            String imgUrl = element0.getAttribute("data-progressive");
                                            Element element1 = (Element) element.getElementsByTagName("h3").item(0);
                                            String head = element1.getTextContent();
                                            Element element2 = (Element) element.getElementsByTagName("em").item(0);
                                            String calendar = element2.getTextContent();
                                            Element element3 = (Element) element.getElementsByTagName("em").item(1);
                                            String location = element3.getTextContent();
                                            *//*LogUtils.e(imgUrl);
                                            LogUtils.e(head);
                                            LogUtils.e(calendar);
                                            LogUtils.e(location);*//*
                                            BingImagesBean bingImagesBean = new BingImagesBean();
                                            bingImagesBean.setImgUrl(imgUrl);
                                            bingImagesBean.setHead(head);
                                            bingImagesBean.setCalendar(calendar);
                                            bingImagesBean.setLocation(location);
                                            beans.add(bingImagesBean);
                                        }
                                    }
                                }
                                iCallback.onSuccess(beans);
                            } catch (Exception e) {
                                iCallback.onFailure(e.getMessage());
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        iCallback.onFailure(anError.getErrorBody());
                    }
             });*/

        Rx2AndroidNetworking.get(RequestUrls.BING_IMAGES)
                .addQueryParameter("p", String.valueOf(page))
                .setTag("getBingImages")
                .build()
                .getStringObservable()
                .map(new Function<String, List<BingImagesBean>>() {
                    @Override
                    public List<BingImagesBean> apply(String html) throws Exception {
                        return buildHtml2Data(html);
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<List<BingImagesBean>>() {
                    @Override
                    public void accept(List<BingImagesBean> bingImagesBeans) throws Exception {
                        iCallback.onSuccess(bingImagesBeans);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        iCallback.onFailure(throwable.getMessage());
                    }
                });
    }


    private List<BingImagesBean> buildHtml2Data(String html) {
        List<BingImagesBean> beans = null;
        try {
            TagNode tagNode = new HtmlCleaner().clean(html);
            org.w3c.dom.Document doc = new DomSerializer(new CleanerProperties()).createDOM(tagNode);
            XPath xpath = XPathFactory.newInstance().newXPath();
            Object object = xpath.evaluate("//div[@class='item']//div[@class='card progressive']", doc, XPathConstants.NODESET);
            if (object instanceof NodeList) {
                NodeList nodes = (NodeList) object;
                beans = new ArrayList<>();
                if (nodes != null && nodes.getLength() > 0) {
                    for (int i = 0; i < nodes.getLength(); i++) {
                        Element element = (Element) nodes.item(i);
                        Element element0 = (Element) element.getElementsByTagName("img").item(0);
                        String imgUrl = element0.getAttribute("data-progressive");
                        Element element1 = (Element) element.getElementsByTagName("h3").item(0);
                        String head = element1.getTextContent();
                        Element element2 = (Element) element.getElementsByTagName("em").item(0);
                        String calendar = element2.getTextContent();
                        Element element3 = (Element) element.getElementsByTagName("em").item(1);
                        String location = element3.getTextContent();
                        BingImagesBean bingImagesBean = new BingImagesBean();
                        bingImagesBean.setImgUrl(imgUrl);
                        bingImagesBean.setHead(head);
                        bingImagesBean.setCalendar(calendar);
                        bingImagesBean.setLocation(location);
                        beans.add(bingImagesBean);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return beans;
    }

}
