package com.theshey.swap.pub.glide;

import android.graphics.Bitmap;
import android.net.Uri;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.Transformation;
import com.bumptech.glide.request.RequestOptions;
import com.theshey.swap.GlideApp;
import com.theshey.swap.R;
import com.theshey.swap.base.BaseApplication;

import static com.bumptech.glide.request.RequestOptions.bitmapTransform;

/**
 * @Author: TheShey
 * @Created at: 2017/7/27 17:15
 * @Email: shilishihe@gmail.com
 * @WeChat: Sonher_Lee
 * @Note: Glide全局管理类
 */
public class GlideManager {
    private static final String ANDROID_RESOURCE = "android.resource://";
    private static final String FOREWARD_SLASH = "/";

    /**
     * 将资源ID转为Uri
     *
     * @param resourceId
     * @return
     */
    public Uri resourceIdToUri(int resourceId) {
        return Uri.parse(ANDROID_RESOURCE + BaseApplication.getThisApp().getPackageName() + FOREWARD_SLASH + resourceId);
    }

    public static void loadUrlImg(ImageView img, String url, Transformation<Bitmap> bitmapTransformation) {
        GlideApp.with(BaseApplication.getThisApp())
                .load(url)
                .centerCrop()
                .error(R.color.font_black_6)
                .placeholder(R.color.font_black_6)
                .optionalTransform(bitmapTransformation)
                .into(img);
    }


}
