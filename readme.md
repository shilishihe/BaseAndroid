#### 项目使用简介
##### 基本功能
 * 网络请求 
 * 各种工具类 
 * 图片加载 
 * 缓存管理
 * 数据库操作
 * 用户行为分析和Crash自动上报
 * 性能分析
 * ...
 
##### 开源库地址
 * XSnow  [项目地址](https://github.com/xiaoyaoyou1212/XSnow)
 * RxPermissions  [项目地址](https://github.com/tbruyelle/RxPermissions)
 * KProgressHUD  [项目地址](https://github.com/Kaopiz/KProgressHUD)
 * BaseRecyclerViewAdapterHelper  [项目地址](https://github.com/CymChad/BaseRecyclerViewAdapterHelper)
 * glide-transformations  [项目地址](https://github.com/wasabeef/glide-transformations)
 * AndroidUtilCode  [项目地址](https://github.com/Blankj/AndroidUtilCode)
 * leakcanary  [项目地址](https://github.com/square/leakcanary)
 * fabric 用户行为分析和Crash上报[项目地址](https://fabric.io)
 * MaterialSearchView 材料设计下的搜索控件[项目地址](https://github.com/MiguelCatalan/MaterialSearchView)